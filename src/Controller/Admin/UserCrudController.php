<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }
    
    public function configureActions(Actions $actions): Actions
    {
        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, Action::DETAIL)
        ->setPermission(Action::NEW, 'ROLE_ADMIN')
        ->setPermission(Action::EDIT, 'ROLE_ADMIN')
        ->setPermission(Action::DELETE, 'ROLE_ADMIN')
        ->setPermission(Action::DETAIL, 'ROLE_ADMIN')
        ;
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
        ->setEntityLabelInSingular('User')
        ->setEntityLabelInPlural('Users')
        ->setSearchFields(['name', 'surname', 'email'])
        ->setDefaultSort(['createdAt' => 'DESC']);
        ;
    }
    
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('user'))
        ;
    }

    public function configureFields(string $pageName): iterable
    {
    
        yield TextField::new('name', 'Imię');
        yield TextField::new('surname', 'Nazwisko');
        yield EmailField::new('email');
        
        yield IntegerField::new('pesel')->hideOnIndex();
        yield TextField::new('nip')->hideOnIndex();
        yield TextField::new('address')->hideOnIndex();
        
        if (Crud::PAGE_EDIT === $pageName) {
           $req = 0;
        } else {
            $req = 1;
        }
        
        yield TextField::new('plainPassword', 'New password')->onlyOnForms()
        ->setFormType(RepeatedType::class)
        ->setFormTypeOptions([
            'type' => PasswordType::class,
            'first_options' => ['label' => 'New password'],
            'second_options' => ['label' => 'Repeat password'],
            'required' => $req,
        ]);
        
        yield TextareaField::new('description')->hideOnIndex();
        yield TextareaField::new('interests')->hideOnIndex();
        yield TextareaField::new('skills')->hideOnIndex();
        yield TextareaField::new('experience')->hideOnIndex();        
        yield IntegerField::new('rating')->hideOnIndex();
        
        yield DateField::new('birthdate')->hideOnIndex();

    }
    
    
    
    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
        
        $this->addEncodePasswordEventListener($formBuilder);
        
        return $formBuilder;
    }
    
    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        
        $this->addEncodePasswordEventListener($formBuilder);
        
        return $formBuilder;
    }
    
    /**
     * @required
     */
    public function setEncoder(UserPasswordEncoderInterface $passwordEncoder): void
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    
    protected function addEncodePasswordEventListener(FormBuilderInterface $formBuilder)
    {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            /** @var User $user */
            $user = $event->getData();
            $user->setRoles(['ROLE_USER']);
            if ($user->getPlainPassword()) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));
            }
        });
    }
    
    
    

}
