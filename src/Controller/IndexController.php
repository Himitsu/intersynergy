<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Entity\User;
use App\Form\UserFormType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class IndexController extends AbstractController
{
    private $passwordEncoder;
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }
    
    /**
     * @Route("/",name="index")
     */
    public function index(Request $request){

        return $this->render('index.html.twig',
        [
         
        ]);
    }
    
    
    /**
     * @Route("/panel",name="panel")
     */
    public function panel(Request $request){
        
        $user = $this->getUser();
        
        return $this->render('panel.html.twig',
        [
            'user' => $user
        ]);
    }
    
    /**
     * @Route("/panel/edit-user",name="edit_user")
     */
    public function editUser(Request $request, string $fileDir){
        
        $user = $this->getUser();
        $form = $this->createForm(UserFormType::class, $user);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('plainPassword')->getData() !== null) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData()));
            }
            
            
            if ($file = $form['file']->getData()) {
                $filename = bin2hex(random_bytes(6)).'.'.$file->guessExtension();
                try {
                    $file->move($fileDir, $filename);
                    
                    // TODO remove old file
                } catch (FileException $e) {
                    // unable to upload the photo, give up
                }
                $user->setfile($filename);
            }
            
            $user->setUpdatedAt(new \DateTime("now"));
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            
            return $this->redirectToRoute('panel');
        }
        
        
        return $this->render('register.html.twig',
            [
                'user_form' => $form->createView(),
                'title' => 'Edit user'
            ]);
    }
    
    
    
    
    /**
     * @Route("/register",name="register")
     */
    public function register(Request $request, string $fileDir){
        
        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('plainPassword')->getData() !== null) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData()));
            }
            
            if ($file = $form['file']->getData()) {
                $filename = bin2hex(random_bytes(6)).'.'.$file->guessExtension();
                try {
                    $file->move($fileDir, $filename);
                } catch (FileException $e) {
                    // unable to upload the photo, give up
                }
                $user->setfile($filename);
            }
            $user->setCreatedAt(new \DateTime("now"));
            $user->setUpdatedAt(new \DateTime("now"));
            $user->setRoles(['ROLE_USER']);
            
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            
            return $this->redirectToRoute('app_login');
        }
        
        
        return $this->render('register.html.twig',
            [
                'user_form' => $form->createView(),
            ]);
    }
    
    
    
    


}