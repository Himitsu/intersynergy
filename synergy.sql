-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Czas generowania: 13 Maj 2021, 17:09
-- Wersja serwera: 8.0.18
-- Wersja PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `synergy`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210512081337', '2021-05-12 08:13:50', 289);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesel` int(11) NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `interests` longtext COLLATE utf8mb4_unicode_ci,
  `skills` longtext COLLATE utf8mb4_unicode_ci,
  `experience` longtext COLLATE utf8mb4_unicode_ci,
  `birthdate` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roles` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `name`, `surname`, `pesel`, `nip`, `address`, `email`, `password`, `description`, `interests`, `skills`, `experience`, `birthdate`, `created_at`, `updated_at`, `rating`, `file`, `roles`) VALUES
(1, 'Harry', 'Potter', 2147483647, '231-321-321-21', 'Hogwart', 'harry.potter@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$ZG1BbzhER0lPRU8wa21mag$5oR+GFNrTMFbXAsNbYDFCUvHSOVPiLjNF3wQ5UpWkAw', '1', '2', '3', '4', '2017-02-16 00:00:00', '2021-05-12 10:19:00', '2021-05-13 09:39:54', 6, '840dcb069154.txt', '[\"ROLE_ADMIN\"]'),
(2, 'Test', 'asdas', 321321, '21321', 'jakastam', 'ania@o2.pl', '$argon2id$v=19$m=65536,t=4,p=1$bzFnZ3JDY3RxaWI1eGpJOQ$u3biFK12dH6gNSP6UQoGoTsHi0eQ2TtXr3lLdPBUzOg', 'da', 'das', 'das', 'das', '2016-01-01 00:00:00', '2021-05-12 13:41:30', '2021-05-12 13:41:30', 5, 'd28688dfdeef.png', '[\"ROLE_USER\"]'),
(5, 'inny', 'sobie', 12321, '3212', 'adress', 'test@o2.pl', '$argon2id$v=19$m=65536,t=4,p=1$WnNqYXdoMi5WNnljak1CUw$n8d5CmQ/QJ3eEKLj7G/J7XiH306kqnIl2H222UlhX1A', NULL, NULL, NULL, NULL, '2021-05-14 00:00:00', '2021-05-13 10:17:39', '2021-05-13 16:57:14', 2, NULL, '[\"ROLE_USER\"]');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
