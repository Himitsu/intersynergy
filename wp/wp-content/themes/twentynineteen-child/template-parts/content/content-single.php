<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( ! twentynineteen_can_show_post_thumbnail() ) : ?>
	<header class="entry-header">
		<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
	</header>
	<?php endif; ?>

	<div class="entry-content">
	
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Post title. Only visible to screen readers. */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentynineteen' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentynineteen' ),
				'after'  => '</div>',
			)
		);
		?>
	
	
	
		<?php if( get_post_type() == 'apiuser'):?>
		<div>
			<div>
				<b>name:</b>
				<span><?php the_field('name'); ?></span>
			</div>
			<div>
				<b>surname:</b>
				<span><?php the_field('surname'); ?></span>
			</div>
			<div>
				<b>pesel:</b>
				<span><?php the_field('pesel'); ?></span>
			</div>
			<div>
				<b>nip:</b>
				<span><?php the_field('nip'); ?></span>
			</div>
			<div>
				<b>address:</b>
				<span><?php the_field('address'); ?></span>
			</div>
			<div>
				<b>email:</b>
				<span><?php the_field('email'); ?></span>
			</div>
			<div>
				<b>description:</b>
				<span><?php the_field('description'); ?></span>
			</div>
			<div>
				<b>interests:</b>
				<span><?php the_field('interests'); ?></span>
			</div>
			<div>
				<b>skills:</b>
				<span><?php the_field('skills'); ?></span>
			</div>
			<div>
				<b>experience:</b>
				<span><?php the_field('experience'); ?></span>
			</div>
			<div>
				<b>birthdate:</b>
				<span><?php echo date('Y-m-d',strtotime(get_field('birthdate')));?></span>
				
				
			</div>
			<div>
				<b>rating:</b>
				<span><?php the_field('rating'); ?></span>
			</div>
			<div>
				<b>file:</b>

				<?php $url = substr(home_url(), 0, -3).'/uploads/';?>
				<span>
					<a href="<?php echo $url.get_field('file')?>"><?php the_field('file'); ?></a>
				</span>
			</div>
		</div>
		<?php endif;?>
	
	
	
		
		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php twentynineteen_entry_footer(); ?>
	</footer><!-- .entry-footer -->

	<?php if ( ! is_singular( 'attachment' ) ) : ?>
		<?php get_template_part( 'template-parts/post/author', 'bio' ); ?>
	<?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
