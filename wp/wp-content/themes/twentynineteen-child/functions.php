<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}


add_action( 'widgets_init', 'ci_register_sidebar' );

function ci_register_sidebar(){
    register_sidebar(array(
        'name' => 'Api User Sidebar',
        'id' => 'api-user-sidebar',
        'description' => 'My sidebar description',
        'before_widget' => '<aside id="%1$s" class="entry-content widget group %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}


function register_apiuser_cpt() {
    register_post_type( 'apiuser', array(
        'label' => 'Api Users',
        'public' => true,
        'capability_type' => 'post',
    ));
}

add_action( 'init', 'register_apiuser_cpt' );

add_action( 'update_user_list', 'get_users_from_api' );
add_action( 'wp_ajax_nopriv_get_users_from_api', 'get_users_from_api' );
add_action( 'wp_ajax_get_users_from_api', 'get_users_from_api' );


function get_users_from_api() {
    // Should return an array of objects
    $results = wp_remote_retrieve_body(wp_remote_get(substr(home_url(), 0, -3).'/api/users', array('headers' => array( 'accept' => 'application/json' ))));
    
    // turn it into a PHP array from JSON string
    $results = json_decode( $results );
    
    // Either the API is down or something else spooky happened. Just be done.
    if( ! is_array( $results ) || empty( $results ) ){
        return false;
    }

    $users[] = $results;
    
    foreach( $users[0] as $user ){
        
        $title = $user->name . ' ' .$user->surname . ' ' . $user->id;
        $user_slug = slugify($title );
        
        $posts = get_posts(array(
            'numberposts'	=> 1,
            'post_type'		=> 'apiuser',
            'meta_key'		=> 'external_id',
            'meta_value'	=> $user->id
        ));
        
        $existing_user = $posts[0];
        
        if( $existing_user === null  ){
            
            $inserted_user = wp_insert_post( [
                'post_name' => $user_slug,
                'post_title' => $title,
                'post_type' => 'apiuser',
                'post_status' => 'publish'
            ] );
            
            if( is_wp_error( $inserted_user ) || $inserted_user === 0 ) {
                // die('Could not insert user: ' . $title);
                // error_log( 'Could not insert user: ' . $title );
                continue;
            }
            
            // add meta fields
            $fillable = [
                'field_609d399876692' => 'id',
                'field_609d3322ed890' => 'name',
                'field_609d3389ed891' => 'surname',
                'field_609d3399ed892' => 'pesel',
                'field_609d33a1ed893' => 'nip',
                'field_609d33a7ed894' => 'address',
                'field_609d33b1ed895' => 'email',
                'field_609d33bced896' => 'description',
                'field_609d33c3ed897' => 'interests',
                'field_609d33c7ed898' => 'skills',
                'field_609d33cbed899' => 'experience',
                'field_609d33d1ed89a' => 'birthdate',
                'field_609d33d7ed89b' => 'rating',
                'field_609d33e8a4cfa' => 'file',
            ];
            
            foreach( $fillable as $key => $name ) {
                update_field( $key, $user->$name, $inserted_user );
            }
            
            
        } else {
            
            $existing_user_id = $existing_user->ID;
            
            
            // update title & slug 
            $inserted_user = wp_insert_post( [
                'ID' => $existing_user_id,
                'post_name' => $user_slug,
                'post_title' => $title,
                'post_type' => 'apiuser',
                'post_status' => 'publish'
            ] );
            
        
            $fillable = [
                'field_609d399876692' => 'id',
                'field_609d3322ed890' => 'name',
                'field_609d3389ed891' => 'surname',
                'field_609d3399ed892' => 'pesel',
                'field_609d33a1ed893' => 'nip',
                'field_609d33a7ed894' => 'address',
                'field_609d33b1ed895' => 'email',
                'field_609d33bced896' => 'description',
                'field_609d33c3ed897' => 'interests',
                'field_609d33c7ed898' => 'skills',
                'field_609d33cbed899' => 'experience',
                'field_609d33d1ed89a' => 'birthdate',
                'field_609d33d7ed89b' => 'rating',
                'field_609d33e8a4cfa' => 'file',
            ];
                
            foreach( $fillable as $key => $name ){
                update_field( $name, $user->$name, $existing_user_id);
            }
        }
        
    }
}


function slugify($text){
    
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    
    // trim
    $text = trim($text, '-');
    
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    
    // lowercase
    $text = strtolower($text);
    
    if (empty($text)) {
        return 'n-a';
    }
    
    return $text;
}

